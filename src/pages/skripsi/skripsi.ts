import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController  } from 'ionic-angular';
import { NgForm } from '../../../node_modules/@angular/forms';
import { DataskripsiPage } from '../dataskripsi/dataskripsi';
/**
 * Generated class for the SkripsiPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-skripsi',
  templateUrl: 'skripsi.html',
})
export class SkripsiPage implements OnInit{

  isChecked=false;
  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl:ModalController) {
  }

  ngOnInit() {
    console.log('ionViewDidLoad SkripsiPage');
  }

  changeValue($event){
    this.isChecked = !this.isChecked; // I am assuming you want to switch between checking and unchecking while clicking on radio.
  }

  submitSkripsi(f:NgForm){
    console.log(f.value);
    console.log(this.isChecked);
    //console.log(f.value.nama);
    let modal = this.modalCtrl.create(DataskripsiPage,{nama:f.value.nama, nim:f.value.nim, judul: f.value.judul, kelas:f.value.optClass,nama1:f.value.nama1, nama2:f.value.nama2,linpro:this.isChecked});
    modal.present();
  }

}
